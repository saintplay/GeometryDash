#include "Winform.h"

namespace GeometryDash
{
	Trampolin::Trampolin(Posicion^ p)
	{
		tipo = Objetos::TrampolinId;
		posicion = gcnew Posicion(p->x, p->y);
	}
	void Trampolin::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Trampolin, getRectanguloParaDibujar());
	}
}