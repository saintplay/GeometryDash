#include "WinForm.h"

namespace GeometryDash
{
	Creditos::Creditos()
	{
		onTimerTick = gcnew EventHandler(this, &Creditos::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &Creditos::teclaDown);
	}

	void Creditos::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			if (!dibujado)
			{
				buffer->Graphics->DrawImage(Imagenes::Creditos, Rectangle(0, 0, Juego::ANCHO_TAMANIO, Juego::ALTURA_TAMANIO));
				buffer->Render(Winform::graphics);
				dibujado = true;
			}
		}
	}

	void Creditos::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter)
			{
				DesactivarEscena(this);
				ActivarEscena(Winform::inicioEscena);
			}
		}
	}
}