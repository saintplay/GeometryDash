#include "Winform.h"

namespace GeometryDash
{
	Espina::Espina(Posicion^ p)
	{
		tipo = Objetos::EspinaId;
		posicion = gcnew Posicion(p->x, p->y);
	}
	void Espina::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Espina, getRectanguloParaDibujar());
	}
}