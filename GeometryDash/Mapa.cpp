#include "Winform.h"

namespace GeometryDash {

	Mapa::Mapa() {
		matriz = gcnew array<Objeto^,2 >(Juego::MAPA_ANCHO, Juego::MAPA_ALTURA);
	}

	Objeto^ Mapa::GetObjetoByCoordenadas(int x, int y) {
		return matriz[x, y];
	}
}
