#include "Winform.h"

namespace GeometryDash
{
	Upecino::Upecino(Posicion^ p)
	{
		imagen = Imagenes::Upecino;
		estado = Estados::Idle;
		posicion = p;
		tiempoDeVuelo = 0;
		velocidadY = 0;
	}

	void Upecino::recalcularPosicion() {
		int gravedad = Juego::gravedad;

		// PRINCIPIOS MATEMATICA Y FISICA

		// MRU
		float siguientePosicionX = (Juego::posicionCamara * 1.0f / Juego::PIXEL_TAMANIO) + 1;
		
		// MRUV
		float diferencialY = ((velocidadY * tiempoDeVuelo) + (0.5 * gravedad * Math::Pow(tiempoDeVuelo, 2))) / Juego::PIXEL_TAMANIO;

		// Se valida una movimiento maximo por segundo
		if (diferencialY <= -1) {
			diferencialY = -0.9;
		}

		float siguientePosicionY = posicion->y + diferencialY;

		int coordenadaX = Math::Floor(siguientePosicionX);
		int coordenadaY = Math::Floor(siguientePosicionY);

		// Aveces por la velocidad de la caida, la coordenada se podr�a salir del mapa (valores negativos)
		// Cuando pase eso, regresamos el valor a cero
		if (coordenadaY < -1) {
			Juego::PerderJuego();
			return;
		}
		else if (coordenadaY < 0) {
			coordenadaY = 0;
		}
		else if (coordenadaY >= Juego::PIXELS_EN_ALTURA) {
			coordenadaY = Juego::PIXELS_EN_ALTURA - 1;
		}
		// FIN DE PRINCIPIOS MATEMATICOS Y FISICOS

		if (posicion->y >= Juego::PIXELS_EN_ALTURA) {
			posicion->y  = Juego::PIXELS_EN_ALTURA - 1;
		}

		// Validar si upecino pisa suelo
		Objeto^ objetoConElQueColisionVerticalmente = Winform::mapa->GetObjetoByCoordenadas(coordenadaX, coordenadaY);
		Objeto^ objetoConElQueColisionHorizontalmente = Winform::mapa->GetObjetoByCoordenadas(coordenadaX, posicion->y);

		if (objetoConElQueColisionHorizontalmente->tipo == PisoId || objetoConElQueColisionHorizontalmente->tipo == EspinaId) {
 			if (estado != Estados::Volando) {
				Juego::PerderJuego();
				return;
			}
		}

		if (objetoConElQueColisionVerticalmente->tipo == PisoId) {
			if (estado == Estados::Volando) {
				Juego::PerderJuego();
				return;
			}

			// Upecino no se cae
			estado = Estados::Idle;

			// Si no cae, reseteamos todas las variables de ca�da
			siguientePosicionY = Math::Floor(posicion->y);
			tiempoDeVuelo = 0;
			velocidadY = 0;
		}
		else if (objetoConElQueColisionVerticalmente->tipo == EspinaId) {
			Juego::PerderJuego();
			return;
		}
		else if (objetoConElQueColisionVerticalmente->tipo == TrampolinId) {
			Saltar(Juego::SALTO_TRAMPOLIN);
			return;
		}
		else if (objetoConElQueColisionVerticalmente->tipo == PortalId) {
			Portal^ portal = dynamic_cast<Portal^>(objetoConElQueColisionVerticalmente);
			if (portal->salida) {
				estado = Estados::Saltando;
			}
			else {
				estado = Estados::Volando;
			}
			return;
		}
		else if (objetoConElQueColisionVerticalmente->tipo == MonedaId) {
			Juego::MONEDAS_CONTADOR += 1;
			Winform::mapa->matriz[coordenadaX, coordenadaY] = gcnew Vacio(gcnew Posicion(coordenadaX, coordenadaY));
			return;
		}
		else // Upecino sigue cayendo
		{
			// Upecino empieza a caer o sigue cayendo
			if (estado == Estados::Volando) {
				estado == Estados::Idle;
			}
			else {
				estado == Estados::Volando;
			}
			
			tiempoDeVuelo += 1;
		}

		posicion->x = siguientePosicionX;
		posicion->y = siguientePosicionY;
	}
	
	void Upecino::MostrarUpecino(Graphics^ graphics)
	{
		if (estado == Estados::Volando) {
			graphics->DrawImage(Imagenes::Nave, getRectanguloParaDibujar());
		}
		else {
			graphics->DrawImage(imagen, getRectanguloParaDibujar());
		}
	}

	void Upecino::Saltar(int velocidad) {
		Juego::SALTOS_CONTADOR += 1;

		if (estado == Estados::Saltando) {
			// return; // Si ya esta saltando, se interrumpe este salto
		}

		if (estado != Estados::Volando) {
			estado = Estados::Saltando;
		}
		tiempoDeVuelo = 0;
		velocidadY = velocidad;
	}
}

