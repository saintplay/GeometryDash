#include "WinForm.h"

namespace GeometryDash
{
	NivelCompletado::NivelCompletado()
	{
		onTimerTick = gcnew EventHandler(this, &NivelCompletado::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &NivelCompletado::teclaDown);
	}

	void NivelCompletado::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			if (!dibujado)
			{
				buffer->Graphics->DrawImage(Imagenes::NivelCompletado, Rectangle(0, 0, Juego::ANCHO_TAMANIO, Juego::ALTURA_TAMANIO));
				buffer->Graphics->DrawString(Juego::INTENTOS_CONTADOR.ToString(), gcnew Font("Arial", 72, FontStyle::Bold), gcnew SolidBrush(Color::White), Point(600, 200));
				buffer->Graphics->DrawString(Juego::TIEMPO_CONTADOR.ToString(), gcnew Font("Arial", 72, FontStyle::Bold), gcnew SolidBrush(Color::White), Point(600, 300));
				buffer->Graphics->DrawString(Juego::SALTOS_CONTADOR.ToString(), gcnew Font("Arial", 72, FontStyle::Bold), gcnew SolidBrush(Color::White), Point(600, 400));
				buffer->Graphics->DrawString((Juego::SALTOS_CONTADOR + Juego::INTENTOS_CONTADOR + Juego::MONEDAS_CONTADOR).ToString(), gcnew Font("Arial", 72, FontStyle::Bold), gcnew SolidBrush(Color::White), Point(700, 500));
				buffer->Render(Winform::graphics);
				dibujado = true;
			}
		}
	}

	void NivelCompletado::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter)
			{
				DesactivarEscena(this);
				int nivel = Winform::juegoEscena->nivel + 1;

				if (nivel == 3) {
					ActivarEscena(Winform::creditosEscena);
				}
				else {
					Nivel::PasarANivel(nivel);
					Juego::INTENTOS_CONTADOR = 0;
					Juego::MONEDAS_CONTADOR = 0;
					Juego::SALTOS_CONTADOR = 0;
					Juego::TIEMPO_CONTADOR = 0;
					ActivarEscena(Winform::juegoEscena);
				}
			}
		}
	}
}