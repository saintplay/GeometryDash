#include "Winform.h"

namespace GeometryDash
{
	Portal::Portal(Posicion^ p, bool salida)
	{
		tipo = Objetos::PortalId;
		this->salida = salida;
		posicion = gcnew Posicion(p->x, p->y);
	}
	void Portal::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Portal, getRectanguloParaDibujar());
	}
}