#include "WinForm.h"

using namespace System;
using namespace System::Media;

namespace GeometryDash
{
	Juego::Juego()
	{
		onTimerTick = gcnew EventHandler(this, &Juego::timerTick);
		onKeyUp = gcnew KeyEventHandler(this, &Juego::teclaUp);

		Winform::upecino = gcnew Upecino(gcnew Posicion(2, 1));
		Juego::posicionCamara = 0;

		SoundPlayer ^musica = gcnew SoundPlayer("Sonidos//Musica.wav");
		musica->PlayLooping();
	}

	void Juego::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			contador++;
			TIEMPO_CONTADOR += 50;
			Winform::winform->Text = "Geometry Dash";

			Winform::upecino->recalcularPosicion(); // Fisica Matematica
			// Obtener el objeto con el cual upecino chocara

			Nivel::MostrarNivel(buffer->Graphics);
			Winform::upecino->MostrarUpecino(buffer->Graphics);

			buffer->Render(Winform::graphics);
			dibujado = true;

			Juego::AvanzarCamara();
		}
	}

	void Juego::AvanzarCamara()
	{
		posicionCamara += velocidadCamara;
	}

	void Juego::teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::Space)
		{
			Winform::upecino->Saltar(Juego::SALTO_NORMAL);
		}
		else if (e->KeyCode == Keys::P)
		{
			DesactivarEscena(this);
			ActivarEscena(Winform::pausaEscena);
		}
	}

	void Juego::GanarJuego()
	{
		Escena::DesactivarEscena(Winform::juegoEscena);
		Escena::ActivarEscena(Winform::nivelCompletadoEscena);
	}

	void Juego::PerderJuego()
	{
		Escena::DesactivarEscena(Winform::juegoEscena);
		Escena::ActivarEscena(Winform::tryAgainEscena);
	}
}

