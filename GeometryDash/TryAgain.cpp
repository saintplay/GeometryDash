#include "WinForm.h"

namespace GeometryDash
{
	TryAgain::TryAgain()
	{
		onTimerTick = gcnew EventHandler(this, &TryAgain::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &TryAgain::teclaDown);
	}

	void TryAgain::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			if (!dibujado)
			{
				buffer->Graphics->DrawImage(Imagenes::TryAgain, Rectangle(0, 0, Juego::ANCHO_TAMANIO, Juego::ALTURA_TAMANIO));
				buffer->Render(Winform::graphics);
				dibujado = true;
			}
		}
	}

	void TryAgain::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter)
			{
				DesactivarEscena(this);
				int nivel = Winform::juegoEscena->nivel;
				Winform::juegoEscena = gcnew Juego();
				Juego::INTENTOS_CONTADOR += 1;
				Juego::MONEDAS_CONTADOR = 0;
				Juego::SALTOS_CONTADOR = 0;
				Juego::TIEMPO_CONTADOR = 0;
				Nivel::PasarANivel(nivel);
				ActivarEscena(Winform::juegoEscena);
			}
		}
	}
}