#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace GeometryDash
{
	public enum Estados { Idle, Saltando, Volando };
	public enum Objetos { VacioId, EspinaId, PisoId, TrampolinId, PortalId, MonedaId };

	public ref class Posicion
	{
	public:
		float x;
		float y;
		Posicion(float pX, float pY);
	};

	public ref class Figura
	{
	public:
		Image^ imagen;
		Posicion^ posicion;
		Figura();
		Rectangle getRectanguloParaDibujar();
	};

	public ref class Upecino : Figura
	{
	public:
		Estados estado;
		int velocidadY;
		int tiempoDeVuelo;

		Upecino(Posicion^ p);
		void recalcularPosicion();
		void MostrarUpecino(Graphics^ graphics);
		void Saltar(int velocidad);
	};
	
	public ref class Objeto : public Figura
	{
	public:
		Objetos tipo;
	};

	public ref class Vacio : public Objeto
	{
	public:
		Vacio(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};
	
	public ref class Piso : public Objeto
	{
	public:
		Piso(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};
	
	public ref class Trampolin : public Objeto
	{
	public:
		Trampolin(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Espina : public Objeto
	{
	public:
		Espina(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Portal : public Objeto
	{
	public:
		Portal(Posicion^ p, bool salida);
		bool salida;
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Moneda : public Objeto
	{
	public:
		Moneda(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Mapa
	{
	public:
		Mapa();
		array<Objeto^, 2>^ matriz;

		Objeto^ GetObjetoByCoordenadas(int x, int y);
	};

	public ref class Nivel
	{
	public:
		Nivel();
		Mapa^ mapa;
		static void PasarANivel(int pNivel);
		static void MostrarNivel(Graphics^ graphics);
	};

	public ref class Nivel1 : public Nivel{public: Nivel1();};
	public ref class Nivel2 : public Nivel{public: Nivel2();};

	public ref class Escena
	{
	public:
		int contador;
		bool activo;
		bool dibujado;
		BufferedGraphics^ buffer;
		KeyEventHandler^ onKeyDown;
		KeyEventHandler^ onKeyUp;
		MouseEventHandler^ onMouseClick;
		EventHandler^ onTimerTick;
		Escena();
		static void CambiarEscena(Escena^ escena);
		static void ActivarEscena(Escena^ escena);
		static void DesactivarEscena(Escena^ escena);
	};

	public ref class Inicio : public Escena
	{
	public:
		Inicio();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class Pausa : public Escena
	{
	public:
		String^ opcion;
		Pausa();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class TryAgain : public Escena
	{
	public:
		TryAgain();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class NivelCompletado : public Escena
	{
	public:
		NivelCompletado();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class Creditos : public Escena
	{
	public:
		Creditos();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class Juego : public Escena
	{
	public:
		static void GanarJuego();
		static void PerderJuego();
		
		static int MAPA_ANCHO = 132; // Pixels en archivo
		static int MAPA_ALTURA = 11; // Pixels en archivo
		
		static int PIXEL_TAMANIO = 60;
		static int PIXELS_EN_ANCHO = 18;
		static int PIXELS_EN_ALTURA = 11;

		static int ANCHO_TAMANIO = PIXEL_TAMANIO * PIXELS_EN_ANCHO;
		static int ALTURA_TAMANIO = PIXEL_TAMANIO * PIXELS_EN_ALTURA;

		static int velocidadCamara = 18; // 18 Pixeles por tick
		static int posicionCamara = 0;

		static int SALTO_NORMAL = 10;
		static int SALTO_TRAMPOLIN = 13;

		static int SALTOS_CONTADOR = 0;
		static int TIEMPO_CONTADOR = 0;
		static int INTENTOS_CONTADOR = 0;
		static int MONEDAS_CONTADOR = 0;

		// TIENE QUE SER DIVIDENDO DEL PIXEL_TAMANIO
		static int gravedad = -2; // 2 Pixeles por tick

		static void AvanzarCamara();
		
		Juego();
		int nivel;

		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class Imagenes
	{
	public:
		Imagenes();
		static Image^ Upecino;
		static Image^ Upecino2;
		static Image^ Upecino3;
		static Image^ Nave;

		static Image^ Espina;
		static Image^ Moneda;
		static Image^ Vacio;
		static Image^ Piso;
		static Image^ Trampolin;
		static Image^ Portal;

		static Image^ Inicio;
		static Image^ Juego;
		static Image^ TryAgain;
		static Image^ NivelCompletado;
		static Image^ Pausa;
		static Image^ Creditos;
	};
}
