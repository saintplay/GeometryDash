#include <fstream>
#include <sstream>
#include "Winform.h"

namespace GeometryDash {

	Nivel :: Nivel() {
		mapa = gcnew Mapa();
	}

	void Nivel::MostrarNivel(Graphics^ graphics)
	{
		int pixelesEnAncho = Juego::PIXELS_EN_ANCHO;
		int pixelesEnAltura = Juego::PIXELS_EN_ALTURA;
		int posicionCamara = Juego::posicionCamara;
		int posicionDeCamaraACoordenadas = Math::Floor(posicionCamara / Juego::PIXEL_TAMANIO);

		int posicionMaxDeCamara = Juego::MAPA_ANCHO - pixelesEnAncho;

		// Validar si ya se llego a la meta
		if (posicionDeCamaraACoordenadas > posicionMaxDeCamara) {
			Juego::GanarJuego();
			return;
		}

		// + 1: Porque necesitamos renderizar un bloque m�s.
		for (int i = 0; i < pixelesEnAncho + 1; i++)
		{
			if (posicionDeCamaraACoordenadas == posicionMaxDeCamara && i == pixelesEnAncho) {
				break;
			}

			for (int j = 0; j < pixelesEnAltura; j++)
			{
				switch (Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j]->tipo)
				{
				case PisoId:
					dynamic_cast<Piso^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				case EspinaId:
					dynamic_cast<Espina^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				case TrampolinId:
					dynamic_cast<Trampolin^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				case VacioId:
					dynamic_cast<Vacio^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				case PortalId:
					dynamic_cast<Portal^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				case MonedaId:
					dynamic_cast<Moneda^>(Winform::mapa->matriz[i + posicionDeCamaraACoordenadas, j])->MostrarSprite(graphics);
					break;
				}
			}
		}
	}

	void Nivel::PasarANivel(int pNivel)
	{
		Winform::juegoEscena->nivel = pNivel;
		int pixel = Juego::PIXEL_TAMANIO;

		if (pNivel == 1)
		{
			Winform::nivel1 = gcnew Nivel1();
			std::ifstream file("Niveles//Nivel1.txt");
			std::string line;

			int i = 0;
			int j = 0;

			if (file.is_open())
			{
				while (getline(file, line))
				{
					i = 0;
					std::stringstream stream(line);
					std::string word;
				
					while (getline(stream, word, ','))
					{
						int x = i;
						int y = Juego::MAPA_ALTURA - j - 1;

						if (word.compare("0") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Vacio(gcnew Posicion(x, y));
						}
						else if (word.compare("1") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Piso(gcnew Posicion(x, y));
						}
						else if (word.compare("2") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Espina(gcnew Posicion(x, y));
						}
						else if (word.compare("3") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Trampolin(gcnew Posicion(x, y));
						}
						else if (word.compare("4") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Portal(gcnew Posicion(x, y), false);
						}
						else if (word.compare("5") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Moneda(gcnew Posicion(x, y));
						}
						else if (word.compare("6") == 0) {
							Winform::nivel1->mapa->matriz[x, y] = gcnew Portal(gcnew Posicion(x, y), true);
						}
						i++;
					}
					j++;
				}
			}
			Winform::mapa = Winform::nivel1->mapa;
		}
		else if (pNivel == 2)
		{
			Winform::nivel2 = gcnew Nivel2();
			std::ifstream file("Niveles//Nivel2.txt");
			std::string line;

			int i = 0;
			int j = 0;

			if (file.is_open())
			{
				while (getline(file, line))
				{
					i = 0;
					std::stringstream stream(line);
					std::string word;

					while (getline(stream, word, ','))
					{
						int x = i;
						int y = Juego::MAPA_ALTURA - j - 1;

						if (word.compare("0") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Vacio(gcnew Posicion(x, y));
						}
						else if (word.compare("1") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Piso(gcnew Posicion(x, y));
						}
						else if (word.compare("2") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Espina(gcnew Posicion(x, y));
						}
						else if (word.compare("3") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Trampolin(gcnew Posicion(x, y));
						}
						else if (word.compare("4") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Portal(gcnew Posicion(x, y), false);
						}
						else if (word.compare("5") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Moneda(gcnew Posicion(x, y));
						}
						else if (word.compare("6") == 0) {
							Winform::nivel2->mapa->matriz[x, y] = gcnew Portal(gcnew Posicion(x, y), 6);
						}
						i++;
					}
					j++;
				}
				Winform::mapa = Winform::nivel2->mapa;
			}
		}
	}

}