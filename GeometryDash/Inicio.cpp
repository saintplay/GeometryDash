#include "WinForm.h"

namespace GeometryDash
{
	Inicio::Inicio()
	{
		onTimerTick = gcnew EventHandler(this, &Inicio::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &Inicio::teclaDown);
	}

	void Inicio::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			buffer->Graphics->DrawImage(Imagenes::Inicio, Rectangle(0, 0, Juego::ANCHO_TAMANIO, Juego::ALTURA_TAMANIO));
			buffer->Render(Winform::graphics);
			dibujado = true;
		}
	}

	void Inicio::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter)
			{
				DesactivarEscena(this);
				Winform::upecino->imagen = Imagenes::Upecino;
				ActivarEscena(Winform::juegoEscena);
			}
			else if (e->KeyCode == Keys::D1)
			{
				DesactivarEscena(this);
				Winform::upecino->imagen = Imagenes::Upecino;
				ActivarEscena(Winform::juegoEscena);
			}
			else if (e->KeyCode == Keys::D2)
			{
				DesactivarEscena(this);
				Winform::upecino->imagen = Imagenes::Upecino2;
				ActivarEscena(Winform::juegoEscena);
			}
			else if (e->KeyCode == Keys::D3)
			{
				DesactivarEscena(this);
				Winform::upecino->imagen = Imagenes::Upecino3;
				ActivarEscena(Winform::juegoEscena);
			}
		}
	}
}