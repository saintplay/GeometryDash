#include "WinForm.h"

namespace GeometryDash
{
	Pausa::Pausa()
	{
		onTimerTick = gcnew EventHandler(this, &Pausa::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &Pausa::teclaDown);
		opcion = "Continuar";
	}

	void Pausa::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			buffer->Graphics->DrawImage(Imagenes::Pausa, Rectangle(0, 0, 832, 577));
			buffer->Render(Winform::graphics);
			dibujado = true;
		}
	}

	void Pausa::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter || e->KeyCode == Keys::Space)
			{
				DesactivarEscena(this);
				ActivarEscena(Winform::juegoEscena);
			}
		}
	}
}