#include "WinForm.h"

namespace GeometryDash
{
	Winform::Winform(void)
	{
		InitializeComponent();
		winform = this;

		imagenes = gcnew Imagenes();
		graphics = this->CreateGraphics();
		context = BufferedGraphicsManager::Current;
		aleatorio = gcnew Random();

		inicioEscena = gcnew Inicio();
		juegoEscena = gcnew Juego();
		tryAgainEscena = gcnew TryAgain();
		pausaEscena = gcnew Pausa();
		creditosEscena = gcnew Creditos();
		nivelCompletadoEscena = gcnew NivelCompletado();

		cambioDeEscena = false;

		//Empezar el juego
		Escena::ActivarEscena(inicioEscena);
		Nivel::PasarANivel(1);
	}

	Winform::~Winform()
	{
		if (components)
		{
			delete components;
		}
	}

	System::Void Winform::Winform_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::E)
			cambioDeEscena = true;
		else if (e->KeyCode == Keys::D5)
		{
				Escena::CambiarEscena(inicioEscena);
		}
		else if (e->KeyCode == Keys::D6)
		{
				Escena::CambiarEscena(juegoEscena);
		}
		else if (e->KeyCode == Keys::D7)
		{
				Escena::CambiarEscena(nivelCompletadoEscena);
		}
		else if (e->KeyCode == Keys::D8)
		{
				Escena::CambiarEscena(creditosEscena);
		}
	}

	System::Void Winform::Winform_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::E && cambioDeEscena)
			cambioDeEscena = false;
	}
}