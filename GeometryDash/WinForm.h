#pragma once
#include "Directorio.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace GeometryDash 
{
	public ref class Winform : public System::Windows::Forms::Form
	{
	public:
		static Winform^ winform;
		static Imagenes^ imagenes;
		static Graphics^ graphics;
		static BufferedGraphicsContext^ context;
		static Random^ aleatorio;
		static Upecino^ upecino;

		static Mapa^ mapa;

		static Inicio^ inicioEscena;
		static Juego^ juegoEscena;
		static Pausa^ pausaEscena;
		static TryAgain^ tryAgainEscena;
		static NivelCompletado^ nivelCompletadoEscena;
		static Creditos^ creditosEscena;

		static Nivel1^ nivel1;
		static Nivel2^ nivel2;
		bool cambioDeEscena;

		Winform(void);
		~Winform();
	public: System::Windows::Forms::Timer^  timer;


	private: System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code

			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 this->timer = (gcnew System::Windows::Forms::Timer(this->components));
				 this->SuspendLayout();
				 // 
				 // timer
				 // 
				 this->timer->Enabled = true;
				 this->timer->Interval = 50;
				 // 
				 // Winform
				 // 
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(Juego::ANCHO_TAMANIO, Juego::ALTURA_TAMANIO);
				 this->Margin = System::Windows::Forms::Padding(0);
				 this->MaximizeBox = false;
				 this->Name = L"Geometry Dash";
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Geometry Dash";
				 this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Winform::Winform_KeyDown);
				 this->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &Winform::Winform_KeyUp);
				 this->ResumeLayout(false);

			 }
#pragma endregion
	
	private: System::Void Winform_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	
	private: System::Void Winform_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	
	};
}
