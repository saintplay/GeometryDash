#include "Winform.h"

namespace GeometryDash
{
	Moneda::Moneda(Posicion^ p)
	{
		tipo = Objetos::MonedaId;
		posicion = gcnew Posicion(p->x, p->y);
	}
	void Moneda::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Moneda, getRectanguloParaDibujar());
	}
}
