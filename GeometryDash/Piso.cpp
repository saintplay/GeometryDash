#include "Winform.h"

namespace GeometryDash
{
	Piso::Piso(Posicion^ p)
	{
		tipo = PisoId;
		posicion = gcnew Posicion(p->x, p->y);
	}

	void Piso::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Piso, getRectanguloParaDibujar());
	}
}