#include "WinForm.h"

namespace GeometryDash
{
	Imagenes::Imagenes()
	{
		// Personajes
		Imagenes::Upecino = Image::FromFile("Imagenes\\Personajes\\Upecino.png");
		Imagenes::Upecino2 = Image::FromFile("Imagenes\\Personajes\\Upecino2.png");
		Imagenes::Upecino3 = Image::FromFile("Imagenes\\Personajes\\Upecino3.png");
		Imagenes::Nave = Image::FromFile("Imagenes\\Personajes\\Nave.png");

		// Objetos
		Imagenes::Espina = Image::FromFile("Imagenes\\Objetos\\Espina.png");
		Imagenes::Moneda = Image::FromFile("Imagenes\\Objetos\\Moneda.png");
		Imagenes::Vacio = Image::FromFile("Imagenes\\Objetos\\Vacio.png");
		Imagenes::Trampolin = Image::FromFile("Imagenes\\Objetos\\Trampolin.png");
		Imagenes::Portal = Image::FromFile("Imagenes\\Objetos\\Portal.png");
		Imagenes::Piso = Image::FromFile("Imagenes\\Objetos\\Piso.png");

		// Interfaces
		Imagenes::Inicio = Image::FromFile("Imagenes\\Interfaces\\Inicio.png");
		Imagenes::Pausa = Image::FromFile("Imagenes\\Interfaces\\Pausa.png");
		Imagenes::Creditos = Image::FromFile("Imagenes\\Interfaces\\YouWin.png");
		Imagenes::NivelCompletado = Image::FromFile("Imagenes\\Interfaces\\NivelCompletado.png");
		Imagenes::TryAgain = Image::FromFile("Imagenes\\Interfaces\\TryAgain.jpg");
	}

}