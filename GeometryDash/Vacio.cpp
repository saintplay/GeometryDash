#include "Winform.h"

namespace GeometryDash
{
	Vacio::Vacio(Posicion^ p)
	{
		tipo = Objetos::VacioId;
		posicion = gcnew Posicion(p->x, p->y);
	}
	void Vacio::MostrarSprite(Graphics^ graphics)
	{
		graphics->DrawImage(Imagenes::Vacio, getRectanguloParaDibujar());
	}
}